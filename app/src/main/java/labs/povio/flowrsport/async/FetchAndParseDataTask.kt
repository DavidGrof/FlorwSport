package labs.povio.flowrsport.async

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import java.util.*

class FetchAndParseDataTask(private val model: Any) : AbstractFetchDataTask() {
    private var listener: JsonGetTaskListener? = null

    fun setListener(listener: JsonGetTaskListener) {
        this.listener = listener
    }

    override fun doInBackground(vararg params: String): Any? {
        url = params[0]
        super.doInBackground()
        val jelement = JsonParser().parse(requestResult!!)
        if (jelement.isJsonObject) {
            val jsonObject = jelement.asJsonObject
            return parseJsonArr(jsonObject)
        }
        return null
    }

    override fun onPostExecute(`object`: Any?) {
        super.onPostExecute(`object`)
        listener!!.taskCompletionResult(`object`)
    }

    private fun parseJsonObj(jobject: JsonObject): Any? {
        try {
            return Gson().fromJson(jobject, model.javaClass)
        } catch (e: UnsupportedOperationException) {
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }

        return null
    }

    private fun parseJsonArr(jobject: JsonObject): ArrayList<Any>? {
        try {
            val arrJson = jobject.getAsJsonArray("flowers")
            val arrayList = ArrayList<Any>()
            for (jsObj in arrJson) {
                arrayList.add(parseJsonObj(jsObj.asJsonObject)!!)
            }
            return arrayList
        } catch (e: UnsupportedOperationException) {
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }
        return null
    }

    interface JsonGetTaskListener {
        fun taskCompletionResult(`object`: Any?)
    }
}
