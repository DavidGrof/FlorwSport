package labs.povio.flowrsport.async

import android.os.AsyncTask
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

abstract class AbstractFetchDataTask : AsyncTask<String, Void, Any>() {
    var requestResult: String? = null
    var url: String? = null

    override fun doInBackground(vararg params: String): Any? {
        if (isCancelled)
            return ""

        requestResult = loadData()
        return requestResult
    }

    // load data
    private fun loadData(): String? {
        val client = OkHttpClient()
        val request = Request.Builder()
                .url(url!!)
                .build()
        try {
            val response = client.newCall(request).execute()
            return response.body()!!.string()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
        return null
    }
}