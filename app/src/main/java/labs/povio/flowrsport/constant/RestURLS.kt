package labs.povio.flowrsport.constant

object RestURLS {

    object GET{
        const val FLOWERS = "https://flowrspot-api.herokuapp.com/api/v1/flowers"
        const val SEARCH_FLOWERS = "https://flowrspot-api.herokuapp.com/api/v1/flowers/search?query="
    }
}