package labs.povio.flowrsport.activity

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import labs.povio.flowrsport.R
import labs.povio.flowrsport.adapter.RecyclerViewAdapter
import labs.povio.flowrsport.async.FetchAndParseDataTask
import labs.povio.flowrsport.constant.RestURLS
import labs.povio.flowrsport.model.FlowerData
import java.util.*
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager


class MainActivity : BaseActivity(), RecyclerViewAdapter.ItemClickListener, FetchAndParseDataTask.JsonGetTaskListener {
    private var fetchAndParseDataTask: FetchAndParseDataTask? = null
    private val flowersList = ArrayList<Any>()
    private var adapter: RecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView = recyclerView
        adapter = RecyclerViewAdapter(flowersList, this, R.layout.flower_row)
        adapter!!.setClickListener(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        if (flowersList.size == 0) {
            fetchAndParseDataTask = FetchAndParseDataTask(FlowerData(1))
            fetchAndParseDataTask!!.setListener(this)
            fetchAndParseDataTask!!.execute(RestURLS.GET.FLOWERS)
        }
        searchEt.setOnEditorActionListener { v, _, _ -> searchFlowers(v) }
    }

    private fun searchFlowers(editText: TextView): Boolean {
        clear()
        fetchAndParseDataTask = FetchAndParseDataTask(FlowerData(1))
        fetchAndParseDataTask!!.setListener(this)
        fetchAndParseDataTask!!.execute(RestURLS.GET.SEARCH_FLOWERS + editText.text)
        return true
    }

    private fun clear() {
        val size = flowersList.size
        if (size > 0) {
            flowersList.removeAll(flowersList)
            adapter!!.notifyItemRangeRemoved(0, size)
        }
    }

    override fun taskCompletionResult(`object`: Any?) {
        try {
            if (`object` != null) {
                if (`object` is ArrayList<*>) {
                    this.flowersList.addAll(`object`)
                }
                val curSize = adapter!!.itemCount
                if (curSize > 1) {
                    adapter!!.notifyItemRangeInserted(curSize, this.flowersList.size - 1)
                } else {
                    adapter!!.notifyItemRangeInserted(curSize, this.flowersList.size)
                }
            }
        } catch (e: ClassCastException) {
            e.printStackTrace()
        }
    }

    override fun onPause() {
        super.onPause()
        fetchAndParseDataTask?.cancel(true)
    }

    override fun onItemClick(view: View, position: Int) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
