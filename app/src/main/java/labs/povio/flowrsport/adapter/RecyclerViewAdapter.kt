package labs.povio.flowrsport.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import labs.povio.flowrsport.R
import labs.povio.flowrsport.model.FlowerData

class RecyclerViewAdapter(private val data: List<*>, context: Context, private val _rowLayout: Int) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    private var clickListener: ItemClickListener? = null
    private val inflater = LayoutInflater.from(context)
    private val glide = Glide.with(context)
    private val requestOptions = RequestOptions().centerCrop()

    inner class ViewHolder
    internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        internal var flowerNameTextView: TextView = itemView.findViewById(R.id.flowerName)
        internal var flowerLatinNameTextView: TextView = itemView.findViewById(R.id.flowerLatinName)
        internal var flowerSightingsTextView: TextView = itemView.findViewById(R.id.flowerSightings)
        internal var imageView: ImageView = itemView.findViewById(R.id.imageView)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            if (clickListener != null) clickListener!!.onItemClick(view, adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(_rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val obj = data[position]
        if (obj is FlowerData) {
            viewHolder.flowerNameTextView.text = obj.name
            viewHolder.flowerLatinNameTextView.text = obj.latin_name
            viewHolder.flowerSightingsTextView.text = obj.sightings.toString()
            if (obj.profile_picture != null && obj.profile_picture.isNotEmpty()) {
                glide.setDefaultRequestOptions(requestOptions)
                        .load("https:" + obj.profile_picture)
                        .into(viewHolder.imageView)
            }
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setClickListener(itemClickListener: ItemClickListener) {
        this.clickListener = itemClickListener
    }

    interface ItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}