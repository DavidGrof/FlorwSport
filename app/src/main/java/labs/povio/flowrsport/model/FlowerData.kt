package labs.povio.flowrsport.model

data class FlowerData(val id: Int) {
    val name: String? = null
    val latin_name: String? = null
    val profile_picture: String? = null
    val sightings: Int = 0
    val favorite: Boolean = false
}
